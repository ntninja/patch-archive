Add PyQt5 Python bindings

 * Upstream project: https://invent.kde.org/packaging/flatpak-kde-runtime
 * Merge request: https://invent.kde.org/packaging/flatpak-kde-runtime/-/merge_requests/32
 * Turned this down since there is also PySide2 (Qt-Python binding “competitor”).
 * Also, it sounded like they primarily view the KDE runtime as a C/C++ thing
   and don’t really care about Python like the Freedesktop runtime does.
 * Suggested creating a PyQt5 base app instead.