Add CC0-1.0 license

 * Upstream project: https://github.com/hroncok/license
 * Pull request: https://github.com/hroncok/license/pull/5
 * Current maintainer is no longer accepting changes.