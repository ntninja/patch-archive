# Archive of dead patches

Archive of all the changes that feel too good to just delete, but that I don’t
have the time and energy to ever attempt to upstream. Some of these were turned
down by upstream, others had requests for changes after I lost interest in
doing the work, many I never even tried.

Still, better to put them here than to just delete them without a trace…

\- Erin
